@extends("layout")

@section("content")
	<div class="row">
		<h2>Todo</h2>
	</div>
	<div class="row">
		<button data-bind="click: refresh" class="two columns">Refresh</button>
		<input type="text" placeholder="What to do..." data-bind="value: title" class="six columns">
		<button data-bind="click: create" class="two columns">Create</button>
		<button data-bind="click: save" class="two columns">Save</button>
	</div>
	<!-- ko foreach: items -->
	<!-- ko ifnot: deleted -->
	<div class="row">
		<div class="one column">
			<a class="fk-btn" data-bind="click: destroy">DELETE</a>
		</div>
		<div class="eleven columns">
			<input type="checkbox" data-bind="checked: completed">
			<span data-bind="editable: title, css: { complete: completed() }"></span>
		</div>
	</div>
	<!-- /ko -->
	<!-- /ko -->
@endsection