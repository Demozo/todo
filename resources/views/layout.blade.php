<!DOCTYPE html>
<html lang="en">
<head>
	<title>ToDo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{ asset("css/normalize.css") }}">
	<link rel="stylesheet" href="{{ asset("css/skeleton.css") }}">
	<link rel="stylesheet" href="{{ asset("css/note.css") }}">
	<link rel="stylesheet" href="{{ asset("css/style.css") }}">
</head>
<body>
<div class="container">
	@yield("content")
</div>
<script src="http://knockoutjs.com/downloads/knockout-3.4.2.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="{{ asset("js/note.js") }}"></script>
<script src="{{ asset("js/app.js") }}"></script>
</body>
</html>