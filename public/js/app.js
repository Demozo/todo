(function() {
	function Item(data) {
		var self = this;

		self.id = data.id;
		self.title = ko.observable(data.title);
		self.deleted = ko.observable(data.deleted);
		self.completed = ko.observable(data.completed);

		self.destroy = function() {
			self.deleted(true);
			axios.post("/api/items/" + self.id, { _method: "DELETE" })
				.catch(function() {
					note.error("Error", "Something went wrong.");
				});
		};

		self.save = function() {
			axios.post("/api/items/" + self.id, {
				title: self.title(),
				completed: self.completed(),
				_method: "PUT"
			})
				.then(function(res) {
					console.log(res.data);
				})
				.catch(function() {
					note.error("Error", "Something went wrong.");
				});
		};
	}

	function TodoApp() {
		var self = this;
		var note = new Note();

		self.items = ko.observableArray();
		self.title = ko.observable("");
		self.create = function() {
			if(self.title().length > 0) {
				if(self.title().length > 191) {
					note.error("Too Long", "Maximum length is 191 characters. Yours is " + self.title().length + " long.");
					return;
				}

				axios.post("/api/items", { title: self.title() })
					.then(function(res) {
						self.items.push(new Item({
							id: res.data.id,
							title: res.data.title,
							deleted: res.data.deleted,
							completed: res.data.completed
						}));

						note.success("Success", "Created new Todo item.");
						self.title("");
					});
			}else{
				note.error("Error", "Please enter a title.");
			}
		};

		self.save = function() {
			console.log("Saving");

			var deletedCount = 0;

			for(var i = 0; i < self.items().length; i++) {
				var item = self.items()[i];

				var data = {
					title: item.title(),
					completed: item.completed(),
					deleted: item.deleted(),
					_method: "PUT"
				};

				if(data.deleted) {
					deletedCount++;
					continue;
				}

				if(data.title.length > 191) {
					note.error("Too Long", "Item nr. " + (i - deletedCount + 1) + " is too long.");
					continue;
				}

				axios.post("/api/items/" + item.id, data)
					.catch(function (err) {
						if(err) {
							note.error("Error", "Something went wrong.");
							console.log(err);
						}
					})
			}
		};

		self.refresh = function() {
			axios.get("/api/items")
				.then(function(res) {
					console.log(res);

					self.items.removeAll();

					for(var i = 0; i < res.data.length; i++) {
						self.items.push(new Item(res.data[i]));
					}
				})
				.catch(function (err) {
					if(err)
						throw err;
				});
		};

		self.refresh();
	}

	ko.bindingHandlers.editable = {
		init: function(element, valueAccessor) {
			var value = valueAccessor();

			function updateModel(){
				// can we write to this observable?
				if (ko.isWriteableObservable(value)) {
					// write to it
					value(this.innerHTML);
				}
			};

			element.innerHTML = value(); // set initial value
			element.contentEditable = true; // make it editable

			// add listeners
			element.addEventListener('blur', updateModel);
			element.addEventListener('keyup', updateModel);
		}
	};

	ko.applyBindings(new TodoApp());
})();