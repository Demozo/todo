<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", function() {
	return view("items.index");
});

Route::prefix("api")->group(function() {
	Route::get("items", "ItemController@index"); // Retrieve all Items
	Route::post("items", "ItemController@store"); // Create new Item
	Route::get("items/{item}", "ItemController@show"); // Get specific Item
	Route::put("items/{item}", "ItemController@update"); // Update specific Item
	Route::delete("items/{item}", "ItemController@destroy"); // Delete specific Item
});